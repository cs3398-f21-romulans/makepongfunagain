# MakePongFunAgain
> The collaborators for this project are Anthony Meza, Bruce Lamb, Bryce Harmon, Jeffrey Pearce, and Ryan Wakabayashi. The team is taking a java pong game and adding extra features like powers or abilities to target the newer generation of gamers. MakePongFunAgain was made to give the new generation the same excitement the world felt in the 70's, and the retrospective feel was intended to be refreshing to the older gamers as well.


> FIXME: Live demo [_here_](https://www.example.com). <!-- If you have the project hosted somewhere, include the link here. -->

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Screenshots](#screenshots)
* [Setup](#setup)
* [Usage](#usage)
* [Project Status](#project-status)
* [Room for Improvement](#room-for-improvement)
* [Acknowledgements](#acknowledgements)
* [Contact](#contact)
<!-- * [License](#license) -->


## General Information
![MakePongFunAgian](https://i.imgur.com/ACCFTxj.jpeg)

- Provide general information about your project here.
  This is a game of pong that allows
  the users to have abilities everytime you score a set number of points
- What problem does it (intend to) solve? This project is a pong game but with abilities.
  It solves
  the problem of pong being dull and boring.
- What is the purpose of your project?
  The purpose of this project is to make a old game new again
  by giving abilites and other functions that the old pong doesn't have.
- Why did you undertake it?
  We took on this project because the team felt it was interesting and
  and challenging enough for the semester.
<!-- You don't have to answer all the questions - just the ones relevant to your project. -->


## Technologies Used
- Tech 1 - version 1.0 - Java 11 and/or 10 


## Features
- Awesome feature 1: add/subtract life - this ability allows the user to take a life from the opponent or add a life
  to oneself.
- Awesome feature 2: Local play - This allows for two players to play locally on the machine that the game is being
  run on.
- Awesome feature 3: make apponent blind - this allows for the player to "blind" the opponent from making contact with
  the ball.
- Awesome feature 4: add decoy balls - this allows the player's ball to create numerous balls that will distract the
  play from the balls trajectory.


## Screenshots
![Example screenshot](./img/screenshot.png)
<!-- If you have screenshots you'd like to share, include them here. -->


## Setup
What are the project requirements/dependencies? Where are they listed? A requirements.txt or a Pipfile.lock file perhaps? Where is it located?

Proceed to describe how to install / setup one's local environment / get started with the project.


## Usage
How does one go about using it?
Provide various use cases and code examples here.

`write-your-code-here`


## Project Status
Project is: _in progress_ / _complete_ / _no longer being worked on_. If you are no longer working on it, provide reasons why.


## Room for Improvement
Include areas you believe need improvement / could be improved. Also add TODOs for future development.

Room for improvement:
- Improvement to be done 1
- Improvement to be done 2

To do:
- Feature to be added 1
- Feature to be added 2


## Acknowledgements
Give credit here.
- This project was inspired by...
- This project was based on [this tutorial](https://www.example.com).
- Many thanks to...


## Contact
Created by [@flynerdpl](https://www.flynerd.pl/) - feel free to contact me!


<!-- Optional -->
<!-- ## License -->
<!-- This project is open source and available under the [... License](). -->

<!-- You don't have to include all sections - just the one's relevant to your project -->
