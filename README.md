# MakePongFunAgain
> The collaborators for this project are Anthony Meza, Bruce Lamb, Bryce Harmon, Jeffrey Pearce, and Ryan Wakabayashi. The team is taking a java pong game and adding extra features like powers or abilities to target the newer generation of gamers. MakePongFunAgain was made to give the new generation the same excitement the world felt in the 70's, and the retrospective feel was intended to be refreshing to the older gamers as well.


> FIXME: Live demo [_here_](https://www.example.com). <!-- If you have the project hosted somewhere, include the link here. -->

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Screenshots](#screenshots)
* [Setup](#setup)
* [Usage](#usage)
* [Project Status](#project-status)
* [Room for Improvement](#room-for-improvement)
* [Acknowledgements](#acknowledgements)
* [Contact](#contact)
<!-- * [License](#license) -->


## General Information
![MakePongFunAgain](https://i.imgur.com/ACCFTxj.jpeg)

- This is a game of pong that allows the users to have abilities everytime you score a set number of points
- This project is a pong game but with abilities. It solves the problem of pong being dull and boring.
- The purpose of this project is to make a old game new again
  by giving abilities and other functions that the old pong doesn't have.
- We took on this project because the team felt it was interesting and challenging enough for the semester.


## Technologies Used
- Tech 1 - version 1.0 - Java 11 and/or 10 
- Tech 2 - version 7.1 - Gradle
- Tech 3 - version 2.0 - CircleCI


## Features
- Awesome feature 1: add/subtract life - this ability allows
  the user to take a life from the opponent or add a life
  to oneself.
- Awesome feature 2: Local play - This allows for two 
  players to play locally on the machine that the game is being
  run on.
- Awesome feature 3: Flashlight - This temporarily blinds
  the opponent by making the ball disappear from the player. Uses a cone formation that makes all pixels white which fundamentally makes the ball disappear.
- Awesome feature 4: Shotgun Ball - this allows the player's
  ball to create numerous balls that will distract the
  play from the balls trajectory.
- Awesome feature 5: Extra Paddles - gives the player a
  number of extra paddles that can block against attack against shotgun ball. 
- Awesome feature 6: Speed/Slow - Speeds up the player's
  movement. Can be player or opponent directed. Can also slow the opponent. 


## Screenshots
FIXME: ![Example screenshot](./img/screenshot.png)
<!-- If you have screenshots you'd like to share, include them here. -->


## Setup
FIXME: What are the project requirements/dependencies? Where are they listed? A requirements.txt or a Pipfile.lock file perhaps? Where is it located?

Proceed to describe how to install / setup one's local environment / get started with the project.


## Usage
FIXME: How does one go about using it?
Provide various use cases and code examples here.

`write-your-code-here`


## Project Status
Project is: _in progress_

The project currently launches to its main menu, but the featured options are not implemented. These options will be implemented after the issues with the menu switching to the game are fixed. While switching from the main menu to the game, the game is instantiated and has a brief load time. After the game loads, the ball will bounce on all four walls and make sounds on the top walls. When the ball bounces against a side wall, the opposite player's points will increase by 1. The paddles' collision still needs implementation. When a point is scored the ball needs to be reset and its starting angle needs to have a random value added. There is a paint flicker issue that needs to be resolved.

- Jeffrey Pearce (update): I have added functionality to let the user scores be populated to the HUD. My code: https://bitbucket.org/cs3398-f21-romulans/makepongfunagain/src/COURTHUD/src/main/java/Courts/CourtHUD.java. I also added the ability for the power icons to be populated based on if the player makes 2 points. Code for that can be found
  here: https://bitbucket.org/cs3398-f21-romulans/makepongfunagain/src/COURTHUD/src/main/java/Courts/CourtHUD.java. I added a refactoring principle that changed the variables of common names to variables of different types.
  I changed "p1Score" to "p2ScoreINT" so it doesn't conflict with p1Score and p2Score. The code can be found here: https://bitbucket.org/cs3398-f21-romulans/makepongfunagain/src/COURTHUD/src/main/java/Courts/CourtHUD.java. Demo with changes can be found here: https://bitbucket.org/cs3398-f21-romulans/makepongfunagain/src/demoBranch/src/main/java/Courts/CourtHUD.java. 

- Jeffrey Pearce (Next step): I need to work with Bryce to check to see if our code is coherent with each other. 

- Ryan Wakabayashi (Status Update): The main menu was added using a workaround that I developed within the [Main_Menu](https://bitbucket.org/cs3398-f21-romulans/makepongfunagain/src/Main_Menu/) branch. Further progress on resetting the round has been stopped and it's waiting for proper implementation of scoring, collisions, and menu navigation.
- Ryan Wakabaysahi (Next step): Refactor of game files to implement static methods that can pause ball movement and reset when switching jpanels.

- Anthony Meza (Status Update): Attempted menu integration using several different techniques. However found a fatal flaw that halted full integration that would require a rework of the enitre project to correctly implement. Removed unnecessary files. Implemented the mainmenu integration fix with ryan which improved functionality and reduced redundant java files with main function. Added functionality to button on CourtHUD. Options menu also cannot be fully implemented until changes are made. Main Menu [integration](https://bitbucket.org/cs3398-f21-romulans/makepongfunagain/commits/branch/Main_Menu), return to menu [button function](https://bitbucket.org/cs3398-f21-romulans/makepongfunagain/src/master/).

- Anthony Meza (Next Step): Rewrite App_PongPlus and others so full intgration will be possible. Work on implementing options menu after rewrite has been done.

- Bruce Lamb (update): Animation is now successfully implemented by the General Ball. Anything containing an image that gets implemented by one of the movie interfaces will also now support animation.
   -Animation Library URL: https://bitbucket.org/cs3398-f21-romulans/makepongfunagain/src/master/src/main/java/AppAnimation/
- Bruce Lamb (update): PongPlusApp package is added for refactoring. Is an absolute must in order to add Listeners directly to the App.
- Bruce Lamb (update): Paddles now move up and down. Although it's great the paddles move, this is an example of an interface requiring too many functions to be implemented. Needed to add functions to the Stadium and Court. The motion did not work if the ActionListeners where added to the JPanels, they had to be added to the JFrame. Constructor needed to pass "itself" (AppPongPlus) instance in order for itself to add the appropriate listeners.
  -Bruce Lamb Paddle Listeners are no longer responsive to the AppPongPlus App. Possibly being added to the wrong Jframe in focus.
  Working commit of paddles
  -Paddle Motion Artifact URL: https://bitbucket.org/cs3398-f21-romulans/makepongfunagain/commits/60ae52a57b56894f5115d208aaadba8e9e34afe1
  -Note:
    -The last working commit hash is: 8c57833
	-Fails at commit hash: 1b9c8fe
	-A JButton was added to the HUD class and only the one file was changed. These are completely independant of each and only have ideas.
	-Java has what is called "Focusable" upon certain components. I can only guess that maybe the focus has changed by default with JButtons.
	-The JButton is completely independant of the paddle listeners and cannot connect the dots on how that breaks the listeners.

  -(more to be added) 

- Bryce Harmon (update): I have added basic scoring functionality to the game. After a player scores a point, the score is the updated and recored in CourtHud.
- Bryce Harmon (next step): finish few remaining aspects of scoring logic such as ending the game at a certain score (most likely 21) and declaring a winner. 
- URL: https://bitbucket.org/cs3398-f21-romulans/makepongfunagain/commits/e453a8e149cf7998614f79e654a2d19c3b5febda

## Room for Improvement
Include areas you believe need improvement / could be improved. Also add TODOs for future development.

Learning some key details of Java:
- Painting and updating images. There is either a lack or misunderstanding of the way Java decides to paint and update. There is some strange behavior that happens when mixing BufferedImage repaint, with non-BufferedImage repaint. It appears you have to stick with one or the other and do it the same everytime. Even when making a menu that doesn't touch the paint engine causes strange behavior such as not appearing except on hover or flicker.
- Java Listeners (who actually "listens?") Knowing beforehand that Java Listeners must be added to JFrames may have impacted the design of this program.

Room for improvement:

- We need more leadership and more meetings
- Improvement to be done 2

To do:

- Write pseudocode for the HUD and abilities
- Feature to be added 2


## Acknowledgements
Give credit here.
- This project was inspired by...

- This project was based on [this tutorial](https://www.example.com).

- StartMenu based on code from:
https://www.zentut.com/java-swing/java-swing-cardlayout/
and
https://www.youtube.com/user/MrJavaHelp/videos?view=0&sort=da&flow=grid

- Many thanks to...


## Contact
Created by [@flynerdpl](https://www.flynerd.pl/) - feel free to contact me!


<!-- Optional -->
<!-- ## License -->
<!-- This project is open source and available under the [... License](). -->

<!-- You don't have to include all sections - just the one's relevant to your project -->
