package PongPlusApp;

import javax.swing.*;

import AppAnimation.*;
import AppImages.*;
import Courts.*;
import Courts.PongObjects.*;
import Courts.BoundryHandlers.*;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;

public class App_PongPlus extends JFrame{
	private App_PongPlus self;
	private JPanel Foundation; //Gives the option for a Menu
	//private CourtHUD M; //The Main start menu reference

	private BufferedImage Buffer;
	private Graphics2D BufferBrush;
	private boolean painting; //is THIS painting

	private Stadium S;
	//Needs a direct link to ALL Court listeners..
	//This is a separate thread that updates the graphics, NO OTHER TASK!
	private Thread PaintEngine;

	private void constructorHelperTestCourt(TestCourt Court,int Width_px,int Height_px){
		//STADIUM SET UP
		//Look at the Hardcoded Numbers
		S=new Stadium(this,Width_px,Height_px);
		S.addHUD(new CourtHUD(Width_px,200), 0,0);
		S.addCourt(Court);

		//Movie Initializer
		TimeSprite ballSprite = new AnimationLibrary().genLiteBall(40,40);
		ballSprite.setTimeInt_msec(250);
		GeneralBall tmpRef = new GeneralBall(20,ballSprite);

		tmpRef.setPos(300, 200); //set this ball as center
		tmpRef.setVelVector(4, 0);
		S.addObjectToCourt(tmpRef,new EasyRandomBallBounce(Court,-45, 45));

		StandardPaddle P1 = new StandardPaddle(
				25,
				100,
				KeyEvent.VK_A,
				KeyEvent.VK_Z);

		StandardPaddle P2 = new StandardPaddle(
				25,
				100,
				KeyEvent.VK_K,
				KeyEvent.VK_M);
		P1.setXpos(50);
		P1.setYpos(200);
		P1.setYvel(5);//WILL Ignore direction

		P2.setXpos(325);
		P2.setYpos(200);
		P2.setYvel(5);//WILL Ignore direction

		S.addObjectToCourt(P1, new StandardPaddleBound());
		S.addObjectToCourt(P2, new StandardPaddleBound());
	}

	public App_PongPlus(int Width_px,int Height_px){
		super();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(null);
		this.setSize(Width_px,Height_px);
		self=this; //Used for anonymous classes

		ImageLibrary ImgL; //temporary resource pull
		ImgL = new ImageLibrary();
		Foundation=new JPanel();
		Foundation.setSize(Width_px,Height_px);
		Foundation.setLocation(0,0);
		Foundation.setLayout(null); //Allows FULL control of location items

		Buffer=new BufferedImage(Width_px,Height_px,BufferedImage.TYPE_INT_ARGB);
		BufferBrush=Buffer.createGraphics();

		BufferedImage originalTMP = ImgL.getMPFA_BG(Width_px,Height_px);

		TestCourt Court = new TestCourt(400,400,originalTMP);
		constructorHelperTestCourt(Court,Width_px, Height_px);
		//

		Foundation.add(S);

		this.add(Foundation); //Finally add the Foundation after all construction is complete
		Foundation.setVisible(true);
		this.setVisible(true); //This CALLS the paint/repaint method
	//	M=new CourtHUD(Width_px,Height_px);
	//	M.setGamePanelSwap(new swapFromMenuToGame());
		//The implementation!!!
		PaintEngine = new Thread(){
			@Override
			public void run(){
				while(true){
					while(!Court.isNextBufferReadyToPaint() && Court.isPainting()){try{Thread.sleep(2);
						}catch(Exception error){System.err.print("FAILS IN PAINT THREAD ENGINE");}
					}
					try{Thread.sleep(18);}
					catch(Exception error){}
					self.repaint();
					Court.moveNextTick();
				}
			}
		};
		
		Court.setDefaultEngine();
		Court.startEngine();
		PaintEngine.start();
	}
	
	//The SOLE PURPOSE of a BufferedImage is to prevent FLicker
	//Very common Graphic practice practice
	public void UpdateBuffer() {
		painting=true;
		S.updateStadiumBuffer();
		S.drawBufferInstance(BufferBrush);
		painting=false;
	}

	@Override
	public void paint(Graphics brush){
		this.UpdateBuffer();
		while(!brush.drawImage(Buffer,0,0,null)){}
	}

	//This class only exists to swap the MainMenu Panel for the game and
	//Start the paint and engine threads correctly.
/*	private class swapFromMenuToGame implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			self.remove(S);
			self.setVisible(false);
			self.dispose();
		}
	}
*/
}
