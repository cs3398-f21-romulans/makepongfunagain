package Courts.Engines;
//This package contains engines that control
//Framerate and boundry control

import Courts.*;

import javax.swing.*;
import java.util.ArrayList;
import java.awt.event.KeyListener;

//This class checks if items are within NON-rotated box. like a window
public class CourtEngine implements Runnable{
	private TestCourt Mother; //Court this engine is attached too
	private long delay_msec;
	private boolean runningNextTick,
					running,
					alive;

	public CourtEngine(TestCourt Court){
		Mother=Court;
		delay_msec = 18;
		running=false;
		alive=true; //legal to run
	}
	
	public void stop() {alive=false;}

	public boolean isRunningNextTick(){return runningNextTick;}
	public boolean isRunning(){return running;}
	public boolean isAlive(){return alive;}

	public void setReadyNextTick(){runningNextTick=true;}
	@Override
	public void run(){
		runningNextTick=running=true;
		int index;

		while(running){
			index=Mother.getObjectCount()-1;
			for(index=Mother.getObjectCount()-1;index>=0;index--){
				if(Mother.isObjectOutBoundTop(index)){Mother.HandleBoundTop(index);}
				else if(Mother.isObjectOutBoundBottom(index)){Mother.HandleBoundBottom(index);}

				if(Mother.isObjectOutBoundLeft(index)){Mother.HandleBoundLeft(index);}
				else if(Mother.isObjectOutBoundRight(index)){Mother.HandleBoundRight(index);}

				if(Mother.isMoveReady(index)){
					Mother.MoveObject(index);
				}
			}

			Mother.UpdateCurrent();
			index=Mother.getObjectCount()-1;
			try {Thread.sleep(delay_msec);
			}catch(Exception error){System.err.print("FAILS IN COURT ENGINE");}
			runningNextTick=false;
			while(!runningNextTick){
				try{Thread.sleep(2);
				}catch(Exception error){System.err.print("FAILS IN COURT ENGINE WAITING ON PRIMARY BUFFER");}
			}
		}
		alive=false;
	}
}