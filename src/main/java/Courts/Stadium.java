package Courts;

import Courts.BoundryHandlers.CourtEdgeRebound;
import PongPlusApp.App_PongPlus;
import Courts.PongObjects.PongObject;
import javax.swing.*;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.EventListener;
import java.awt.event.KeyListener;

//The purpose of this class is to allow HUD and power Panels
//to display relevant information and allow user to move back
//to main menu.
//This will make it very easy for the main menu because the
//main menu will only need to worry about the
public class Stadium extends JPanel{
    private App_PongPlus thisApp; //reference to add listeners directly
    private BufferedImage Buffer;
    private Graphics2D paintBrush;

    private Court C;
    private JPanel Cref; //EXACT same object as C

    private CourtHUD H;

    public Stadium(App_PongPlus theApp, int width_px, int height_px){
        super();
        setLayout(null); //MAY be good for a non-null Layout
        setSize(width_px,height_px);
        setVisible(true);

        thisApp=theApp;
        Buffer=new BufferedImage(width_px,height_px,BufferedImage.TYPE_INT_ARGB);
        paintBrush=(Graphics2D)Buffer.getGraphics();

        //Ugly Pink Background
        for(int i=0;i<width_px;i++){
            for(int j=0;j<height_px;j++){
                Buffer.setRGB(i,j,0xFFFFFFFF);
                //0xffff00ff
            }
        }
    }

    public void addHUD(CourtHUD newHUD,int posX, int posY){
        if(H!=null){this.remove(H);}
        H=newHUD;
        H.setLocation(posX,posY);
        H.setVisible(true);
        this.add(H);
    }

    //Court MUST be a JPanel
    public void addCourt(Court theCourt){
        C=theCourt;
        Cref = (JPanel)theCourt;
        this.add(Cref);
        int w_px=Cref.getWidth(),
            h_px=Cref.getHeight();

        int topLeftX = (this.getWidth()-w_px)/2,
            topLeftY = this.getHeight()-h_px;

        Cref.setLocation(topLeftX,topLeftY);
        Cref.setBounds(topLeftX,topLeftY,w_px,h_px);
    }

    //WARNING: Duplicate code, should be replaced with a "helper" function

    public void addObjectToCourt(PongObject X){
        //Court creates a listner (if any) based on object type
        C.addPongObject(X);

        EventListener L = C.getObjectListener(X); //Super interface
        if(L == null) return;

        if(L instanceof KeyListener){
            thisApp.addKeyListener((KeyListener) L);
        }
    }

    public void addObjectToCourt(PongObject X, CourtEdgeRebound Action){
        //Court creates a listner (if any) based on object type
        C.addPongObject(X, Action);

        EventListener L = C.getObjectListener(X); //Super interface
        if(L == null) return;

        if(L instanceof KeyListener){
            thisApp.addKeyListener((KeyListener) L);
        }
    }

    public int getCourtP1Score(){return ((TestCourt)C).getScoreLeft();}
    public int getCourtP2Score(){return ((TestCourt)C).getScoreRight();}

    public void updateStadiumBuffer(){
        //Paint HUD
        H.updateHUD(this);
        H.paint(paintBrush);

        //Paint Court
        C.drawBufferInstance(paintBrush,Cref.getX(),Cref.getY());

        //Paint Any Additional Panels
    }

    public void drawBufferInstance(Graphics2D paintBrush){
        while(!paintBrush.drawImage(Buffer,0,0,null)){}
    }
}