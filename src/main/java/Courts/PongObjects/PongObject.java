package Courts.PongObjects;

import java.awt.image.BufferedImage;
import java.awt.Graphics2D;

public interface PongObject {
	public boolean isCollided(PongObject X);
	//FOR JAVA PAINTING SIMPLICITY
	public double getXTLpos();
	public double getYTLpos();

	public double getXpos();
	public double getYpos();
	public double getXvel();
	public double getYvel();
	public double getSpeed();

	public boolean isMoveNext();

	public String getTypeName();
	
	public void setXpos(double X_px);
	public void setYpos(double Y_px);
	public void setPos(double X_px, double Y_px);
	public void setXvel(double dx_px_dt);
	public void setYvel(double dy_px_dt);
	public void setVelVector(double Speed, double Angle_deg);

	public void drawInstance(Graphics2D paintBrush,int Xpos_px,int Ypos_px);

	public void move(); //move 1 IAW velocity
}