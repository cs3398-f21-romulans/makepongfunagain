package Courts.PongObjects;

import java.util.EventListener;

public interface Paddle extends PongObject{
    public int getWidth();
    public int getHeight();
    public EventListener getController();
    public void setUpKey(int upKeyCode);
    public void setDownKey(int downKeyCode);
}