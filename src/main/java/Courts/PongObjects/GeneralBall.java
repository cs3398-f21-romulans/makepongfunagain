package Courts.PongObjects;

import AppAnimation.*;

import java.awt.*;
import java.awt.image.*;

public class GeneralBall implements Ball{
	private Movie Img;
	private double XTLpos, YTLpos, //NOTE: TOP LEFT
				   XCpos, YCpos, //NOTE: Center (X,Y)
				   Xvel, Yvel, speed,
				   R; //Radius

	public GeneralBall(double Radius, BufferedImage I) {
		R=Radius;
		if(I==null){
			BufferedImage tmp=new BufferedImage((int)R*2,(int)R*2,BufferedImage.TYPE_INT_ARGB);
			Graphics2D paintBrush = (Graphics2D)tmp.getGraphics();
			paintBrush.setColor(new Color(128,255,0));
			paintBrush.fillOval(0, 0, tmp.getWidth(), tmp.getHeight());
			Img = new SingleImage(tmp);
		}else Img=new SingleImage(I);

		XTLpos=YTLpos=-R;
		XCpos=YCpos=Xvel=Yvel=0;
	}

	public GeneralBall(double Radius, TimeSprite movieSprite){
		R=Radius;
		Img=movieSprite;
		XTLpos=YTLpos=-R;
		XCpos=YCpos=Xvel=Yvel=0;
	}

	public double getRadius(){return R;}
	public double getDiameter(){return R+R;}
	@Override
	public boolean isCollided(PongObject X){
		//TODO WIll write WAY later collide with other objects
		return false;
	}

	//PONG OBJECT OVERRIDES
	@Override
	public double getXTLpos(){return XTLpos;}

	@Override
	public double getYTLpos() {return YTLpos;}

	@Override
	public double getXpos() {return XCpos;}

	@Override
	public double getYpos() {return YCpos;}

	@Override
	public double getXvel() {return Xvel;}

	@Override
	public double getYvel() {return Yvel;}
	
	@Override
	public double getSpeed() {return speed;}

	@Override
	public boolean isMoveNext(){return true;} //the ball is ALWAYS moving (..well this one is..)

	@Override
	public String getTypeName(){return "GeneralBall";}

	@Override
	public void setXpos(double PosX_px){
		XCpos=PosX_px;
		XTLpos=PosX_px-R;
	}

	@Override
	public void setYpos(double PosY_px){
		YCpos=PosY_px;
		YTLpos=PosY_px-R;
	}

	public void setPos(double PosX_px, double PosY_px){
		setXpos(PosX_px);
		setYpos(PosY_px);
	}
	
	@Override
	public void setXvel(double Xvel_pxPerClock){
		Xvel=Xvel_pxPerClock;
		speed = Math.sqrt(Xvel*Xvel+Yvel*Yvel);
	}

	@Override
	public void setYvel(double Yvel_pxPerClock){
		Yvel=Yvel_pxPerClock;
		speed = Math.sqrt(Xvel*Xvel+Yvel*Yvel);
	}

	@Override
	public void setVelVector(double Speed, double Angle_deg){
		double rad = Angle_deg*Math.PI/180;
		speed = Speed;
		Xvel = speed *Math.cos(rad);
		Yvel = speed *Math.sin(rad);
	}

	@Override
	public void drawInstance(Graphics2D paintBrush,int Xpos_px,int Ypos_px){
		Img.setActiveByTime(System.currentTimeMillis());
		Img.drawInstance(paintBrush,Xpos_px,Ypos_px);
	}

	@Override
	public void move(){
		XTLpos+=Xvel;
		XCpos+=Xvel;
		YTLpos+=Yvel;
		YCpos+=Yvel;
	}
}