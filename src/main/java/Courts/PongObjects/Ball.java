package Courts.PongObjects;

public interface Ball extends PongObject{
	public double getRadius();
	public double getDiameter();
}