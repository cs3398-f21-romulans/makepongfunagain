package Courts.PongObjects;

import AppAnimation.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.EventListener;

//Standard Paddle ONLY Moves up and down
public class StandardPaddle implements Paddle{
    private StandardPaddle self;
    private double width, height,
                   xPos, yPos,
                   yVel,
                   MaxSpeed;

    private boolean moving;

    private KeyListener controller;
    private SingleImage Img;

    private void ConstructorHelper(
            int width_px,
            int height_px,
            int keyCodeforUP,
            int keyCodeforDOWN){
        self=this;
        width=width_px;
        height=height_px;
        xPos=yPos=yVel=MaxSpeed=0;
        moving=false;
        controller = new Controller(keyCodeforUP, keyCodeforDOWN);

        BufferedImage tmp = new BufferedImage(width_px,height_px,BufferedImage.TYPE_INT_ARGB);
        for(int x=0;x<tmp.getWidth();x++){
            for(int y=0;y<tmp.getHeight();y++){
                tmp.setRGB(x,y,0xffffff00);
            }
        }

        Img=new SingleImage(tmp);
    }

    public StandardPaddle(int width_px,int height_px){
        ConstructorHelper(width_px,height_px,KeyEvent.VK_UP,KeyEvent.VK_DOWN);
    }

    public StandardPaddle(
            int width_px,
            int height_px,
            int keyCodeforUP,
            int keyCodeforDOWN
    ){
        ConstructorHelper(width_px,height_px,keyCodeforUP,keyCodeforDOWN);
    }

    @Override
    public int getWidth(){
        return (int)width;
    }

    @Override
    public int getHeight(){
        return (int)height;
    }

    @Override
    public boolean isCollided(PongObject X){return false;}

    @Override
    public double getXTLpos() {return getXpos();}

    @Override
    public double getYTLpos(){return getYpos();}

    @Override
    public double getXpos(){return xPos;}

    @Override
    public double getYpos(){return yPos;}

    @Override
    public double getXvel(){return 0;}

    @Override
    public double getYvel(){return yVel;}

    @Override
    public double getSpeed(){return Math.abs(yVel);}

    @Override
    public boolean isMoveNext(){return moving;}

    @Override
    public String getTypeName(){return "StandardPaddle";}

    @Override
    public EventListener getController(){return controller;}

    @Override
    public void setUpKey(int upKeyCode){}

    @Override
    public void setDownKey(int downKeyCode){}

    @Override
    public void setXpos(double X_px){xPos=X_px;}

    @Override
    public void setYpos(double Y_px){yPos=Y_px;}

    @Override
    public void setPos(double X_px, double Y_px){
        xPos=X_px;
        yPos=Y_px;
    }

    @Override
    public void setXvel(double dx_px_dt){}

    @Override
    public void setYvel(double dy_px_dt){
        MaxSpeed=dy_px_dt;
    }

    @Override
    public void setVelVector(double Speed, double Angle_deg){}

    @Override
    public void drawInstance(Graphics2D paintBrush, int Xpos_px, int Ypos_px) {
        Img.drawInstance(paintBrush,Xpos_px,Ypos_px);
    }

    @Override
    public void move(){
        System.out.println("DEBUG: Move paddle called PRE yPos: "+yPos);
        System.out.println("DEBUG: Speed: "+yVel);
        this.yPos+=yVel;
        System.out.println("DEBUG: Move paddle called yPos: "+yPos);
    }

    private class Controller implements KeyListener{
        private int up,
                    down;

        private Controller(int upKeyCode, int downKeyCode){
            this.up=upKeyCode;
            this.down=downKeyCode;
        }

        @Override
        public void keyTyped(KeyEvent e){}

        @Override
        public void keyPressed(KeyEvent e){
            System.out.println("DEBUG: Button detected");
            if(e.getKeyCode() == up){
                System.out.println("DEBUG: pressing up");
                self.yVel=-MaxSpeed;
                self.yPos += self.yVel;
                moving=true;
            }else if(e.getKeyCode() == down){
                System.out.println("DEBUG: pressing down");
                self.yVel =MaxSpeed;
                self.yPos += self.yVel;
                moving=true;
            }else{
                //self.yVel=0;
                moving=false;
            }

        }

        @Override
        public void keyReleased(KeyEvent e){
            System.out.println("DEBUG: button released");
            yVel=0;
            moving=false;
        }
    }
}