package Courts;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.Random;
import MainMenu.startMenu;
import java.io.IOException;


public class CourtHUD extends JPanel{
    private BufferedImage Buffer;
    private Graphics2D P;
    private int p1ScoresINT = 0, p2ScoresINT = 0; //Will be deleted when Bryce has completed his scoring
    private String p1Txt, p2Txt;
    private JLabel P1scoreDisplay;
    private JLabel P2scoreDisplay;
   // private JButton tmpExit;

    public CourtHUD(int width_px,int height_px){
        super();
        this.setLayout(null);
        this.setSize(width_px,height_px);

        Buffer=new BufferedImage(width_px,height_px,BufferedImage.TYPE_INT_ARGB);
        P=(Graphics2D)Buffer.getGraphics();
        P.setColor(new Color(255,255,255));
/*
        tmpExit=new JButton("Main Menu");
        tmpExit.setSize(150,50);
        tmpExit.setLocation(230,35);
        tmpExit.setVisible(true);
        tmpExit.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        try { new startMenu();}
                        catch (IOException f) {
                            f.printStackTrace();}

                    }
                }
        );

        // Had to remove returntoMenu button b/c it crashes paddles

        //this.add(tmpExit);
        */

        p1Txt="Player 1 Score: ";
        p2Txt="Player 2 Score: ";
	    String[] icons = {"0","<|", "|", "+", "-", ":", ">>", "<<"};
        int noPower = 0;
        //INTITIALIZE NON PRIMITIVE TYPES IN CONSTRUCTOR NOT HERE
        //int p1ScoresINT = TestCourt.getScoreLeft();
        //int p2ScoresINT = TestCourt.getScoreRight();
        //int p1ScoresINT = 2, p2ScoresINT = 1; //Will be deleted when Bryce has completed his scoring
        //int p1ScoresINT = 0, p2ScoresINT = 0; //Will be deleted when Bryce has completed his scoring
        int userPower1 = 2, userPower2 = 2;
        int x = 1, y = 6;
        Random random = new Random();

        // makes a random int that is between 0-6 to access power icons
        if (p1ScoresINT % 2 == 0) {
            userPower1 = random.nextInt(7);
            if (userPower1 == 0){
                userPower1 += 1;
            }
        }
        else {
            userPower1 = noPower;
        }

        // makes a random int that is between 0-6 to access power icons
        if (p2ScoresINT % 2 == 0) {
            userPower2 = random.nextInt(7);
            if (userPower2 == 0){
                userPower2 += 1;
            }
        }
        else {
            userPower2 = noPower;
        }

        //String p1Score = Integer.toString(p1ScoresINT);
        //String p2Score = Integer.toString(p2ScoresINT);

        //Player 1 score
        P1scoreDisplay = new JLabel();
        //JLabel p1ScoreIndex = new JLabel(p1Score);
        //player1Score.setForeground(Color.black);
        //p1ScoreIndex.setForeground((Color.black));
        //player1Score.setSize(210,100);
        //p1ScoreIndex.setSize(100,100);
        //player1Score.setLocation(95,100);
        //p1ScoreIndex.setLocation(200,100);
        //player1Score.setVisible(true);
        //p1ScoreIndex.setVisible(true);
        //this.add(player1Score);
        P1scoreDisplay.setSize(210,100);
        P1scoreDisplay.setLocation(95,100);
        P1scoreDisplay.setVisible(true);
        this.add(P1scoreDisplay);
        //this.add(p1ScoreIndex);

        //Player 2 score
        P2scoreDisplay = new JLabel();
        //JLabel p2ScoreIndex = new JLabel(p2Score); //Player Score number
        //player2Score.setForeground(Color.black);
        //p2ScoreIndex.setForeground((Color.black));
        //player2Score.setSize(200,100);
        //p2ScoreIndex.setSize(100,100);
        //player2Score.setLocation(400,100);
        //p2ScoreIndex.setLocation(505,100); //Player Score number
        //player2Score.setVisible(true);
        //p2ScoreIndex.setVisible(true);
        //this.add(player2Score);
        P2scoreDisplay.setSize(210,100);
        P2scoreDisplay.setLocation(400,100);
        P2scoreDisplay.setVisible(true);
        this.add(P2scoreDisplay);
        //this.add(p2ScoreIndex);

        // Player 1 Power
        JLabel player1Pwr = new JLabel("Power: ");
        JLabel popPlayerPwr1 = new JLabel(icons[userPower1]);
        player1Pwr.setForeground(Color.black);
        popPlayerPwr1.setForeground(Color.black);
        player1Pwr.setSize(100,100);
        popPlayerPwr1.setSize(100,100);
        player1Pwr.setLocation(95,120);
        popPlayerPwr1.setLocation(200,120);
        player1Pwr.setVisible(true);
        popPlayerPwr1.setVisible(true);
        this.add(player1Pwr);
        this.add(popPlayerPwr1);

        // Player 2 Power
        JLabel player2Pwr = new JLabel("Power: ");
        JLabel popPlayerPwr2 = new JLabel(icons[userPower2]);
        player2Pwr.setForeground(Color.black);
        popPlayerPwr2.setForeground(Color.black);
        player2Pwr.setSize(100,100);
        popPlayerPwr2.setSize(100, 100);
        player2Pwr.setLocation(400,120);
        popPlayerPwr2.setLocation(505, 120);
        player2Pwr.setVisible(true);
        popPlayerPwr2.setVisible(true);
        this.add(player2Pwr);
        this.add(popPlayerPwr2);

        this.setVisible(true);
    }

    void updateHUD(Stadium ParentStadium){
        this.P1scoreDisplay.setText(p1Txt+ParentStadium.getCourtP1Score());
        this.P2scoreDisplay.setText(p2Txt+ParentStadium.getCourtP2Score());

        P.fillRect(0,0,this.getWidth(),this.getHeight());
        for(Component X: this.getComponents()){
            JComponent JX = (JComponent)X;
            JX.paint(P);
        }
    }

 //   public void setGamePanelSwap(ActionListener L){tmpExit.addActionListener(L);}

}
