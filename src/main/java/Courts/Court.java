package Courts;

import Courts.BoundryHandlers.CourtEdgeRebound;
import Courts.PongObjects.*;
import java.awt.Graphics2D;
import java.util.EventListener;

//This is the game with a running Engine
public interface Court{
	public int getObjectCount();
	public String getObjectType(int IndexValue);
	public boolean isEngineSet();

	//Will this Object move on THIS game tick
	public boolean isMoveReady(int index);

	public void addPongObject(PongObject X);
	public void addPongObject(PongObject X, CourtEdgeRebound Action);
	public void startEngine();
	//SHOULD HAVE A STOP ENGINE;

	public boolean isPainting();
	public boolean isNextBufferReadyToPaint(); //signal to the primary paintbrush to paint image
	public EventListener getObjectListener(PongObject X);
	public void drawBufferInstance(Graphics2D paintBrush);
	public void drawBufferInstance(Graphics2D paintBrush,int posX, int PosY);

	public void moveNextTick();
	//Will move the object on THIS game tick
	public void MoveObject(int index);

	public void HandleBoundTop(int index);
	public void HandleBoundBottom(int index);
	public void HandleBoundLeft(int index);
	public void HandleBoundRight(int index);
}