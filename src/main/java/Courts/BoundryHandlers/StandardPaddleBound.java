package Courts.BoundryHandlers;

import Courts.PongObjects.PongObject;

public class StandardPaddleBound implements CourtEdgeRebound{
    @Override
    public void ReboundTop(PongObject X) {

    }

    @Override
    public void ReboundBottom(PongObject X) {

    }

    @Override
    public void ReboundLeft(PongObject X) {}

    @Override
    public void ReboundRight(PongObject X) {}
}
