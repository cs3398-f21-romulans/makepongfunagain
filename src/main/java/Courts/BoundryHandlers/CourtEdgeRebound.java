package Courts.BoundryHandlers;

import Courts.PongObjects.*;

public interface CourtEdgeRebound{
	void ReboundTop(PongObject X);
	void ReboundBottom(PongObject X);
	void ReboundLeft(PongObject X);
	void ReboundRight(PongObject X);
}