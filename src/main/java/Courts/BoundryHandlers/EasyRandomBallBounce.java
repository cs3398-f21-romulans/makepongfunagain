package Courts.BoundryHandlers;

import Courts.*;
import Courts.PongObjects.*;
import Courts.Sounds.PongBlip;

import javax.sound.sampled.Clip;
import java.util.Random;

public class EasyRandomBallBounce implements CourtEdgeRebound{
	private TestCourt Mother;
	private Random R;
	private double Angle_deg,
				   diff, offset;

	private Ball BX; //prevents unnecessary Reference construction in loops

	//SHOULD be initialized in the CONSTRUCTOR, not here.
	PongBlip sound = new PongBlip("./src/main/resources/bounce.wav");

	public void callSound(){
		sound.blip();
	}

	public static int score1 = 0;
	public static int score2 = 0;

	//MUST be between -180 and +180 LeftLimit < RightLimit
	public EasyRandomBallBounce(TestCourt MotherCourt,double leftLimit_deg,double rightLimit_deg){
		Mother=MotherCourt;
		R = new Random();
		Angle_deg = 0;
		diff = Math.abs(leftLimit_deg)+Math.abs(rightLimit_deg);
		offset = leftLimit_deg;
	}

	//PongObject MUST be a Ball
	@Override
	public void ReboundTop(PongObject X) {
		BX = (Ball)X;
		BX.setYvel(-BX.getYvel());
		BX.setYpos((int)BX.getRadius());
		callSound();
	}

	@Override
	public void ReboundBottom(PongObject X){
		BX = (Ball)X;
		BX.setYvel(-BX.getYvel());
		BX.setYpos((int)(Mother.getHeight()-BX.getRadius()));
		callSound();
	}
	
	@Override
	public void ReboundLeft(PongObject X){
		BX=(Ball)X;
		Angle_deg = R.nextDouble()*diff+offset;
		BX.setVelVector(BX.getSpeed(),Angle_deg);
		BX.setXvel(Math.abs(BX.getXvel()));
		BX.setXpos((int)BX.getRadius());
		score2++;
		Mother.setScoreRight(score2);
	}

	@Override
	public void ReboundRight(PongObject X){
		BX=(Ball)X;
		Angle_deg = R.nextDouble()*diff+offset;
		BX.setVelVector(BX.getSpeed(),Angle_deg);
		BX.setXvel(-Math.abs(BX.getXvel()));
		BX.setXpos((int)(Mother.getWidth()-BX.getRadius()));
		score1++;
		Mother.setScoreLeft(score1);
	}
}