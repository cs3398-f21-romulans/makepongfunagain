package Courts;

import AppAnimation.*;
import Courts.Engines.*;
import Courts.BoundryHandlers.*;
import Courts.PongObjects.*;
import javax.swing.*;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.Color;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;;
import java.util.EventListener;
import java.awt.event.*;

public class TestCourt extends JPanel implements Court{ //SHOULD BE ABSTRACT on
	private TestCourt self;
	private SingleImage BG;

	private BufferedImage Buffer;
	private Graphics2D BufferBrush;
	private boolean painting; //is painting in progress

	private CourtThread E; //NOTE: MUST pass a CourtEngine upon construction
	private CourtEngine Eng; //MUST be the SAME instance parameter of Thread E!!

	//Parallel Arrays
	//For FullProof, all of these arrays Should be bundled into a class!
	private ArrayList<PongObject> ItemL;
	private ArrayList<PongListenMap> ListenerList;
	private ArrayList<RectBoundries> Bounds;
	private ArrayList<CourtEdgeRebound> ReboundHandler;

	private int score1;
	private int score2;

	public void setScoreLeft(int score1) {
		this.score1 = score1;
	}

	public void setScoreRight(int score2) {
		this.score2 = score2;
	}

	public int getScoreLeft() {
		return score1;
	}

	public int getScoreRight() {
		return score2;
	}

	public TestCourt(int Width_px, int Height_px,Image Background){
		super();
		setSize(Width_px,Height_px);
		setLayout(null);

		self = this;
		BufferedImage newBG = new BufferedImage(Width_px,Height_px,BufferedImage.TYPE_INT_ARGB);
		Buffer = new BufferedImage(Width_px,Height_px,BufferedImage.TYPE_INT_ARGB);
		Graphics2D newBGBrush = (Graphics2D)newBG.getGraphics();
		BufferBrush = (Graphics2D)Buffer.getGraphics();

		if(Background==null){
			newBGBrush.setColor(new Color(255,0,255)); //Ugly Pink Background
			newBGBrush.fillRect(0,0,Width_px,Height_px);
		}else{
			AffineTransform T = new AffineTransform();
			double scaleWidth, scaleHeight; //TMP variables
			scaleWidth = Width_px/(double)Background.getWidth(null);
			scaleHeight = Height_px/(double)Background.getHeight(null);

			T.scale(scaleWidth,scaleHeight);
			while(!newBGBrush.drawImage(Background,T,null)){}
		}
		BG=new SingleImage(newBG);

		for(int x=0; x<newBG.getWidth(); x++){
			for(int y=0; y<newBG.getHeight(); y++){
				Buffer.setRGB(x, y, newBG.getRGB(x, y));
			}
		}

		ItemL=new ArrayList<PongObject>();
		ListenerList=new ArrayList<PongListenMap>();
		Bounds=new ArrayList<RectBoundries>();
		ReboundHandler=new ArrayList<CourtEdgeRebound>();
		setVisible(true);

		score1=score2=0;
	}

	private void addObjHelper(PongObject X) {
		ItemL.add(X);
		if(X instanceof Ball) {
			ListenerList.add(new PongListenMap(X,null));
			Ball BX = (Ball)X;
			RectBoundries B = new RectBoundries(
				BX.getRadius(),self.getHeight()-BX.getRadius(),
				BX.getRadius(),self.getWidth()-BX.getRadius()
			);
			Bounds.add(B);
		}else if(X instanceof Paddle){
			Paddle PX = (Paddle)X;
			ListenerList.add(new PongListenMap(X,PX.getController()));
			self.addKeyListener((KeyListener)PX.getController());
			RectBoundries B = new RectBoundries(
				0,self.getHeight()-PX.getHeight(),
				0,self.getWidth()-PX.getWidth()
			);
			Bounds.add(B);
		}
	}
	
	@Override
	public void addPongObject(PongObject X){
		addObjHelper(X);
		ReboundHandler.add(null); //Add to preserve Parallelism
	}

	@Override
	public void addPongObject(PongObject X, CourtEdgeRebound Action){
		addObjHelper(X);
		ReboundHandler.add(Action); //Add to preserve Parallelism
	}
	
	public void setReboundHandlerToPongObject(int index, CourtEdgeRebound Handler){
		ReboundHandler.set(index, Handler);
	}

	public void setDefaultEngine(){
		E=new CourtThread(new CourtEngine(this));
	}
	
	@Override
	public void startEngine(){E.start();}

	@Override
	public void moveNextTick(){E.CE_Instance.setReadyNextTick();}

	@Override
	public void MoveObject(int index){ItemL.get(index).move();}

	//Updated Buffer to the current state
	public void UpdateCurrent(){
		painting=true;
		BG.drawInstance(BufferBrush,0,0);
		for(PongObject X : ItemL){
			X.drawInstance(BufferBrush,(int)X.getXTLpos(),(int)X.getYTLpos());
		}
		painting=false;
	}

	//Needs updated once flicker is setup properly
	@Override
	public boolean isPainting(){return false;}

	//NOTE: Clarification: is External classes ready to safely paint?
	@Override
	public boolean isNextBufferReadyToPaint(){return !E.isRunningNextTick();}

	@Override
	public void drawBufferInstance(Graphics2D paintBrush){
		while(!paintBrush.drawImage(Buffer,0,0,null)){}
	}

	@Override
	public void drawBufferInstance(Graphics2D paintBrush,int posX, int posY){
		while(!paintBrush.drawImage(Buffer,posX,posY,null)){}
	}

	@Override
	public EventListener getObjectListener(PongObject X){
		//check if EXACT object exists
		for(int i=0;i<ListenerList.size();i++) {
			if(X == ListenerList.get(i).O){
				return ListenerList.get(i).L;
			}
		}
		return null; //NOT found OR no Listener necessary
	}

	@Override
	public int getObjectCount() {return ItemL.size();}
	
	@Override
	public String getObjectType(int IndexValue) {return ItemL.get(IndexValue).getTypeName();}

	@Override
	public boolean isMoveReady(int index){return ItemL.get(index).isMoveNext();}

	public boolean isObjectOutBoundTop(int index){return Bounds.get(index).isOutUpper(ItemL.get(index));}
	public boolean isObjectOutBoundBottom(int index){return Bounds.get(index).isOutLower(ItemL.get(index));}
	public boolean isObjectOutBoundLeft(int index){return Bounds.get(index).isOutLeft(ItemL.get(index));}
	public boolean isObjectOutBoundRight(int index){return Bounds.get(index).isOutRight(ItemL.get(index));}

	@Override
	public boolean isEngineSet(){
		if(E==null) return false;
		else if(E.isAlive()==false){
			E=null;
			return false;
		}else return true;
	}

	//Keep the Objects ON THE COURT
	@Override
	public void HandleBoundTop(int index){ReboundHandler.get(index).ReboundTop(ItemL.get(index));}
	
	@Override
	public void HandleBoundBottom(int index){ReboundHandler.get(index).ReboundBottom(ItemL.get(index));}
	
	@Override
	public void HandleBoundLeft(int index){ReboundHandler.get(index).ReboundLeft(ItemL.get(index));}
	
	@Override
	public void HandleBoundRight(int index){ReboundHandler.get(index).ReboundRight(ItemL.get(index));}

	//Each Object has Court Boundries based
	//on it's geometric shape
	//Because these shapes are simple, Upright rectangles will be used.
	//Will need advanced algorithms if we are using stars and concave shapes.
	private class CourtThread extends Thread{
		CourtEngine CE_Instance;

		public CourtThread(Runnable CourtEngineRunnable){
			super(CourtEngineRunnable);
			CE_Instance = (CourtEngine)CourtEngineRunnable;
		}

		private boolean isRunningNextTick(){return CE_Instance.isRunningNextTick();}
	}

	private class RectBoundries{
		private double T, B,
					   L, R;

		private RectBoundries(double t, double b, double l, double r){
			T = t;
			B = b;
			L = l;
			R = r;
		}

		public boolean isOutUpper(PongObject Obj) {
			return Obj.getYpos() < T;
		}

		public boolean isOutLower(PongObject Obj) {
			return Obj.getYpos() > B;
		}

		public boolean isOutLeft(PongObject Obj) {
			return Obj.getXpos() < L;
		}

		public boolean isOutRight(PongObject Obj) {
			return Obj.getXpos() > R;
		}
	}

	private class PongListenMap{
		private PongObject O;
		private EventListener L;

		private PongListenMap(PongObject Object, EventListener Listener){
			O=Object;
			L=Listener;
		}
	}
}