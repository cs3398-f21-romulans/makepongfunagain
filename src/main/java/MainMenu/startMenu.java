package MainMenu;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.File;
import javax.imageio.ImageIO;
import java.io.IOException;
import java.awt.image.BufferedImage;
import PongPlusApp.App_PongPlus;


public class startMenu {

	public startMenu() throws IOException {
		
        final JPanel cards; //a panel that uses CardLayout
        
        // button commands
        final String START = "START";
        final String OPTIONS = "OPTIONS";
        final String MAINMENU = "MAINMENU";
        final String[] diffLvls = {"Easy","Normal","Hard"};

        JFrame frame = new JFrame("Pong Plus");

        JComboBox diffyBox;
        JRadioButton fullScreenButton;
        JRadioButton windowedButton;        
        JSlider frameRateSlider;
        
        frameRateSlider = new JSlider(30,120,60);
        diffyBox = new JComboBox(diffLvls);
        
        BufferedImage pongball = ImageIO.read(new File("./src/main/resources/pongb1.png"));
        JLabel pongball_img = new JLabel(new ImageIcon(pongball));
        BufferedImage pongtable = ImageIO.read(new File("./src/main/resources/pongt2.jpg"));
        JLabel pongtable_img = new JLabel(new ImageIcon(pongtable));
        
        fullScreenButton = new JRadioButton("Full Screen");
        windowedButton = new JRadioButton("Windowed");
        
        ButtonGroup optsGroup = new ButtonGroup();
        optsGroup.add(fullScreenButton);
        optsGroup.add(windowedButton);
        
       //Create the "cards".
        JPanel card1 = new JPanel();
        JLabel title0=new JLabel("Pong Plus");
        card1.add(title0);
        card1.setLayout(null);
        title0.setBounds(150, -95, 350, 250);
        title0.setFont(new Font("Serif", Font.BOLD, 70));
        card1.setBackground(Color.RED.darker().darker());
        card1.add(pongball_img);
        pongball_img.setBounds(200, 150, 200, 200);
        card1.add(pongtable_img);
        pongtable_img.setBounds(50, 125, 450, 300);
        
        // Where the main game will be located
        JPanel card2 = new JPanel();
        card2.setLayout(null);
        card2.setBackground(Color.green);
        JLabel gameMessage = new JLabel("(Game Displayed Here)");
        gameMessage.setFont(new Font("Serif", Font.PLAIN, 50));
        gameMessage.setBounds(60, 150, 480, 150);
        card2.add(gameMessage);
        
        // Where the options panel is located
        JPanel card3 = new JPanel();
        JLabel title1 = new JLabel("Options");
        card3.add(title1);
        card3.setLayout(null);
        title1.setBounds(225, -95, 350, 250);
        title1.setFont(new Font("Serif", Font.PLAIN, 50));
        card3.setBackground(Color.orange);
        card3.add(diffyBox);
        diffyBox.setBounds(325, 115, 100, 25);
        JLabel option1 = new JLabel("Difficulty Level: ");
        card3.add(option1);
        option1.setBounds(100, 100, 275, 50);
        option1.setFont(new Font("Serif", Font.PLAIN, 30));
        JLabel option2 = new JLabel("Screen size: ");
        option2.setFont(new Font("Serif", Font.PLAIN, 30));
        option2.setBounds(100, 200, 275, 50);
        fullScreenButton.setBounds(375, 215, 95, 25);
        windowedButton.setBounds(280, 215, 95, 25);
        card3.add(fullScreenButton);
        card3.add(windowedButton);
        card3.add(option2);    
        frameRateSlider.setPaintTicks(true);
        frameRateSlider.setPaintTrack(true);
        frameRateSlider.setPaintLabels(true);
        frameRateSlider.setMinorTickSpacing(5);
        frameRateSlider.setMajorTickSpacing(15);
        JLabel option3 = new JLabel("Frame Rate: "+frameRateSlider.getValue());
        card3.add(option3);
        option3.setBounds(45, 300, 200, 50);
        option3.setFont(new Font("Serif", Font.PLAIN, 30));
        card3.add(frameRateSlider);
        frameRateSlider.setBounds(250, 300, 300, 50);
        
        //Create the panel that contains the "cards".
        cards = new JPanel(new CardLayout());
        cards.add(card1);
        cards.add(card2);
        cards.add(card3);

        
       class ControlActionListenter implements ActionListener {
            public void actionPerformed(ActionEvent e) {
                CardLayout cl = (CardLayout) (cards.getLayout());
                String cmd = e.getActionCommand();
                if (cmd.equals(START)) {
                    new App_PongPlus(600,600);
                    frame.setVisible(false);
                    // cl.next(cards);
                } else if (cmd.equals(OPTIONS)) {
                    cl.last(cards);
                } else if (cmd.equals(MAINMENU)) {
                    cl.first(cards);
                }
            }
        }
                
        ControlActionListenter cal = new ControlActionListenter();

        JButton btn1 = new JButton("Start new game");
        btn1.setActionCommand(START);
        btn1.addActionListener(cal);

        JButton btn2 = new JButton("Options");
        btn2.setActionCommand(OPTIONS);
        btn2.addActionListener(cal);

        JButton btn3 = new JButton("Main Menu");
        btn3.setActionCommand(MAINMENU);
        btn3.addActionListener(cal);
        
        JButton btn4 = new JButton("Main Menu");
        btn4.setActionCommand(MAINMENU);
        btn4.addActionListener(cal);
      
        card1.add(btn1);
        card1.add(btn2);
        btn1.setBounds(125, 475, 150, 25);
        btn2.setBounds(350, 475, 100, 25);

        card2.add(btn3);
        btn3.setBounds(250, 525, 100, 25);

        card3.add(btn4);
        btn4.setBounds(250, 525, 100, 25);
        
        Container pane = frame.getContentPane();
        pane.add(cards);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600, 600);
        frame.setVisible(true);
	}

}
