package AppAnimation;

import java.awt.image.BufferedImage;
import java.awt.Graphics2D;

//Will have classes the implement sound but not yet
public interface Movie{
    public void drawInstance(Graphics2D paintBrush,int Xpos_px, int Ypos_px);
    public BufferedImage getInstanceCopy();
    public void setActiveByTime(double anyTime_mSec);
}