package AppAnimation;

import java.awt.image.BufferedImage;
import java.awt.Graphics2D;

//Instances of this class should serve teh purpose of a buffer
public class SingleImage implements Movie{
    private BufferedImage Img;
    private Graphics2D Brush;

    public SingleImage(BufferedImage nonNullCopy){
        Img=new BufferedImage(
            nonNullCopy.getWidth(),
            nonNullCopy.getHeight(),
            BufferedImage.TYPE_INT_ARGB
        );

        Brush=(Graphics2D)Img.getGraphics();

        for(int x=0;x<Img.getWidth();x++){for(int y=0;y<Img.getHeight();y++){Img.setRGB(x,y,nonNullCopy.getRGB(x,y));}}
    }

    @Override
    public void drawInstance(Graphics2D paintBrush, int Xpos_px, int Ypos_px){
        paintBrush.drawImage(Img,Xpos_px,Ypos_px,null);
    }

    @Override
    public BufferedImage getInstanceCopy(){
        BufferedImage R = new BufferedImage(Img.getWidth(),Img.getHeight(),BufferedImage.TYPE_INT_ARGB);
        for(int x=0;x<Img.getWidth();x++){
            for(int y=0;y<Img.getHeight();y++){
                R.setRGB(x,y,Img.getRGB(x,y));
            }
        }
        return R;
    }

    //NOTE: SUPPOSED TO BE EMPTY: do not alter
    @Override
    public void setActiveByTime(double relativeTime_mSec){}
}