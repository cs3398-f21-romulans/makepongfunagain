package AppAnimation;

//The purpose of this Library is to build preemptive images
import AppImages.ImageLibrary;

import java.awt.*;
import java.awt.image.BufferedImage;

public class AnimationLibrary{

    //Light because will only use top row
    public TimeSprite genLiteBall(int width_px, int height_px){
        final int NUM_OF_SPRITES=8;
        final long COLOR_KEY= 0x00ff00ff;
        int spriteWidth=62,  //pixels
            spriteHeight=61; //pixels

        BufferedImage spriteSheet = new ImageLibrary().getBilliardXBallSpriteSheet();
        BufferedImage Element[]=new BufferedImage[NUM_OF_SPRITES];
        Graphics2D tmpP;

        //Turn COLORK_KEY to transparent
        int filteredColorPix;
        for(int x=0;x<spriteSheet.getWidth();x++){
            for(int y=0;y<spriteSheet.getHeight();y++){
                //Flush transparent value
                filteredColorPix=spriteSheet.getRGB(x,y) & 0x00ffffff;
                if(filteredColorPix==COLOR_KEY){spriteSheet.setRGB(x,y,0);}
            }
        }

        int xStart[]= new int[NUM_OF_SPRITES];
        int yStart[]= new int[NUM_OF_SPRITES];

        //initiallize start x location CAREFULLY check sprite sheet
        //THESE HARDCODES MUST be "eyeballed" on pixel location.
        //Top left coordinates
        xStart[0]=0;    yStart[0]=0;
        xStart[1]=256;  yStart[1]=0;
        xStart[2]=0;    yStart[2]=64;
        xStart[3]=256;  yStart[3]=64;
        xStart[4]=0;    yStart[4]=128;
        xStart[5]=256;  yStart[5]=128;
        xStart[6]=0;    yStart[6]=192;
        xStart[7]=256;  yStart[7]=192;

        for(short i=0;i< Element.length;i++){
            Element[i]=new BufferedImage(spriteWidth,spriteHeight,BufferedImage.TYPE_INT_ARGB);
            tmpP=(Graphics2D)Element[i].getGraphics();
            while(!tmpP.drawImage(
                    spriteSheet,
                    0, 0,
                    spriteWidth, spriteHeight,
                    xStart[i],yStart[i],
                    xStart[i]+spriteWidth,yStart[i]+spriteHeight,
                    null)
            ){}
        }

        return new TimeSprite(Element,width_px,height_px);
    }
}