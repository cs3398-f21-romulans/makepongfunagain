package AppAnimation;

import java.util.ArrayList;
import java.awt.image.BufferedImage;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

public class TimeSprite implements Movie{
    private ArrayList<BufferedImage> Sprite;
    BufferedImage Active;
    double timeInt_msec,
           movieLength_msec;

    public TimeSprite(BufferedImage[] imageArr,int width_px,int height_px){
        //Note imageArr must NOT have null elements
        //width_px and height_px are individual sprite sizes
        Sprite = new ArrayList<BufferedImage>();
        timeInt_msec=1000;
        AffineTransform scale;
        BufferedImage newTmp;
        Graphics2D tmpP;
        for(BufferedImage X : imageArr){
            scale = new AffineTransform();
            scale.setToScale(
            (double)width_px/X.getWidth(),
            (double)height_px/X.getHeight()
            );

            newTmp=new BufferedImage(width_px,height_px,BufferedImage.TYPE_INT_ARGB);
            tmpP=(Graphics2D)newTmp.getGraphics();
            while(!tmpP.drawImage(X,scale,null)){}
            Sprite.add(newTmp);
        }
        movieLength_msec=timeInt_msec*Sprite.size();
    }

    public TimeSprite(BufferedImage nonNullCopy){
        Sprite=new ArrayList<BufferedImage>();
        Active=new BufferedImage(nonNullCopy.getWidth(),nonNullCopy.getHeight(),BufferedImage.TYPE_4BYTE_ABGR);

        //Direct pixel alteration
        for(int x=0;x<Active.getWidth();x++){for(int y=0;y<Active.getWidth();y++){Active.setRGB(x,y,nonNullCopy.getRGB(x,y));}}
        Sprite.add(Active);

        timeInt_msec=movieLength_msec=1000; //not relevant to single images
    }

    public TimeSprite(){
        Sprite=new ArrayList<BufferedImage>();
        Active=null;
        timeInt_msec=0;
    }

    public void setTimeInt_msec(double time_msec){timeInt_msec=time_msec;}

    @Override
    public void drawInstance(Graphics2D paintBrush,int Xpos_px, int Ypos_px){
        paintBrush.drawImage(Active,Xpos_px,Ypos_px,null);
    }

    @Override
    public BufferedImage getInstanceCopy(){
        return null;
    }

    @Override
    public void setActiveByTime(double anyTime_mSec){
        Active=Sprite.get((int)((anyTime_mSec/timeInt_msec)%Sprite.size()));
    }
}