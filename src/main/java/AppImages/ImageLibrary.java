package AppImages;

import java.awt.Toolkit; //Smart Hardware to software stuff
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform; //used to scale image

//The purpose of this class is to load Image files and return BufferedImages
//This is the ONLY package that should directly have BufferedImages
//Every function should have some HARD CODED reference to a file name.

//This SHOULD be static but Toolkit does not allow static reference
public class ImageLibrary{
    private Toolkit t=Toolkit.getDefaultToolkit();
    private boolean isDrawing; //allows threads to check if an image is being drawn

    private final String RESOURCE_PATH = "../"; //relative path to resource folder

    public ImageLibrary(){
        Toolkit t=Toolkit.getDefaultToolkit();
        isDrawing=false;
    }

    public boolean isDrawing(){return isDrawing;}

    public BufferedImage getMPFA_BG(int width_px, int height_px){
        isDrawing=true;
        Image original = t.getImage(this.getClass().getResource(RESOURCE_PATH+"game_background.jpeg"));
        while(original.getWidth(null) <= 0 || original.getHeight(null) <= 0){}

        BufferedImage R;
        Graphics2D brush;
        if(width_px<=0 || height_px<=0){
            R=new BufferedImage(original.getWidth(null),original.getHeight(null),BufferedImage.TYPE_INT_ARGB);
            brush=(Graphics2D)R.getGraphics();
            while(!brush.drawImage(original,0,0,null)){} //do nothing loop until drawn correctly
            isDrawing=false;
            return R;
        }
        AffineTransform scale = new AffineTransform(); //scaling magic
        scale.scale(
                (double)width_px/original.getWidth(null),
                (double)height_px/original.getHeight(null)
        );
        R=new BufferedImage(width_px,height_px,BufferedImage.TYPE_INT_ARGB);
        brush=(Graphics2D)R.getGraphics();
        while(!brush.drawImage(original,scale,null)){} //do nothing loop until drawn
        isDrawing=false;
        return R;
    }

    public BufferedImage getBilliardXBallSpriteSheet(){
        //width and height are individual sprite widths and heights
        isDrawing=true;
        Image original = t.getImage(
            this.getClass().getResource(RESOURCE_PATH+"UnprocessedSprites/BallSprites/billiardXBall.png")
        );
        while(original.getWidth(null) < 0 || original.getHeight(null) < 0){}
        BufferedImage R=new BufferedImage(
            original.getWidth(null),
            original.getHeight(null),
            BufferedImage.TYPE_INT_ARGB
        );

        Graphics2D tmpP=(Graphics2D)R.getGraphics();
        while(!tmpP.drawImage(original,0,0,null)){}
        return R;
    }
}